/* File: example.i */
%module example

%{
/* Put header files here or function declarations like below */
#define SWIG_FILE_WITH_INIT
#include "example.hpp"
%}

extern int fact(int n);
extern int my_mod(int x, int y);
extern char *get_time();