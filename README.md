# SWIGTEST1

https://www.gitbook.com/book/david-estevez/tutorial-swig
http://www.swig.org/tutorial.html

## Python

To generate the wrapper source files:

    $ swig -c++ -python example.i

To compile the Python module:

    $ python setup.py build_ext --inplace

Test out in python

    $ python
    Python 3.5.2 |Anaconda custom (64-bit)| (default, Jul  2 2016, 17:53:06) 
    [GCC 4.4.7 20120313 (Red Hat 4.4.7-1)] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> import example
    >>> example.fact(4)
    >>> example.my_mod(11,3)
    >>> example.get_time()
    >>> quit()

