/* File: example.hpp */

#ifndef EXAMPLE_HPP
#define EXAMPLE_HPP

extern int fact(int n);
extern int my_mod(int x, int y);
extern char *get_time();

#endif // EXAMPLE_HPP

	
